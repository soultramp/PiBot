
#Ta = Abtastzeit
#Kp = Verstärkungsfaktor (ermitteln start 50)
#Ki = Regelungsfaktor (ermitteln start 0,2)
#w = soll
#x = ist
#esum = esum + e
#Kd = Regelungsfaktor
#ealt = e
#e = w - x


class Pid:

    # set values for PID

    def __init__(self):

        global e_sum
        global e_alt
        global m_left_new
        global m_right_new

        e_alt = 0
        e_sum = 0
        m_left_new = 0
        m_right_new = 0

    def set_motors(self, nano, m_left, m_right):

        kp = 0.0005
        ki = 0.001
        kd = 0.0001
        Ta = 0.0001

        global e_sum
        global e_alt
        global m_left_new
        global m_right_new

        val_should = m_left - m_right
        e_left, e_right = nano.get_encoders()
        val_is = e_left - e_right
        e = val_should - val_is
        e_sum = e_sum + e

        val_corr = kp * e + ki * Ta * e_sum + kd * (e - e_alt) / Ta
        e_alt = e

        m_left_new += val_corr

        nano.set_motors(int(m_left + m_left_new), int(m_right + m_right_new))
